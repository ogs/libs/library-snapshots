# Library snapshots

## From Software Heritage

Direct downloads are not possible yet. We store pre-downloaded snapshot in this repository.

For snapshots ids replace `_` with `:`:

```
swh_1_dir_1cfa7d45f0ec8c0f72674b2258bc5f0705203975
                      |
                      V
swh:1:dir:1cfa7d45f0ec8c0f72674b2258bc5f0705203975
```

Get to the origin by searching for the identifier at https://archive.softwareheritage.org/browse/search.
